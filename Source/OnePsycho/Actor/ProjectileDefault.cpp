﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileDefault.h"

#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Kismet/GameplayStatics.h"
#include "Perception/AISense_Damage.h"
#include "Net/UnrealNetwork.h"

// Sets default values
AProjectileDefault::AProjectileDefault()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// NetWork
	SetReplicates(true);

	/* ---   Collision Sphere   --- */
	BulletCollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Collision Sphere"));

	BulletCollisionSphere->SetSphereRadius(16.f);

	BulletCollisionSphere->bReturnMaterialOnMove = true;

	BulletCollisionSphere->SetCanEverAffectNavigation(false);

	RootComponent = BulletCollisionSphere;
	// ----------------------------------------------------------------------------------------------------



	/* ---   Bullet Setting   --- */
	BulletMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Bullet Projectile Mesh"));
	BulletMesh->SetupAttachment(RootComponent);
	BulletMesh->SetCanEverAffectNavigation(false);

	BulletFX = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Bullet FX"));
	BulletFX->SetupAttachment(RootComponent);

	//BulletSound = CreateDefaultSubobject<UAudioComponent>(TEXT("Bullet Audio"));
	//BulletSound->SetupAttachment(RootComponent);
	// ----------------------------------------------------------------------------------------------------



	/* ---   Projectile Movement   --- */
	BulletProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Bullet Projectile Movement"));
	BulletProjectileMovement->UpdatedComponent = RootComponent;
	BulletProjectileMovement->InitialSpeed = 1.f;
	BulletProjectileMovement->MaxSpeed = 0.;

	BulletProjectileMovement->bRotationFollowsVelocity = true;
	BulletProjectileMovement->bShouldBounce = true;
	// ----------------------------------------------------------------------------------------------------
}

// Called when the game starts or when spawned
void AProjectileDefault::BeginPlay()
{
	Super::BeginPlay();

	BulletCollisionSphere->OnComponentHit.AddDynamic(this, &AProjectileDefault::BulletCollisionSphereHit);
	BulletCollisionSphere->OnComponentBeginOverlap.AddDynamic(this, &AProjectileDefault::BulletCollisionSphereBeginOverlap);
	BulletCollisionSphere->OnComponentEndOverlap.AddDynamic(this, &AProjectileDefault::BulletCollisionSphereEndOverlap);
}

// Called every frame
void AProjectileDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}
// ----------------------------------------------------------------------------------------------------



/* ---   Projectile Info   --- */
/* Инициализация:
*  1.	Установка начальных данных
*  2.	Удаление компонентов, зависимых от "ProjectileSetting"
*/
void AProjectileDefault::InitProjectile(FProjectileInfo InitParam)
{
	BulletProjectileMovement->InitialSpeed = InitParam.ProjectileInitSpeed;
	BulletProjectileMovement->MaxSpeed = InitParam.ProjectileMaxSpeed;

	LifeSpan(InitParam);

	if (InitParam.ProjectileStaticMesh)
	{
		InitVisualMeshProjectile_Multicast(InitParam.ProjectileStaticMesh, InitParam.ProjectileStaticMeshOffset);
	}
	else
		BulletMesh->DestroyComponent(true);

	if (InitParam.ProjectileTrailFX)
	{
		InitVisualTrailProjectile_Multicast(InitParam.ProjectileTrailFX, InitParam.ProjectileTrailFXOffset);
	}
	else
		BulletFX->DestroyComponent(true);

	ProjectileSetting = InitParam;
}



// Ограничение времени существования текущего объекта
void AProjectileDefault::LifeSpan(FProjectileInfo InitParam)
{
	this->SetLifeSpan(InitParam.ProjectileLifeTime);
}
// ----------------------------------------------------------------------------------------------------



/* ---   Hit   --- */
void AProjectileDefault::BulletCollisionSphereHit(
	UPrimitiveComponent* HitComp,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	FVector NormalImpulse,
	const FHitResult& Hit)
{
	if (OtherActor && Hit.PhysMaterial.IsValid())
	{
		EPhysicalSurface mySurfaceType = UGameplayStatics::GetSurfaceType(Hit);

		// Отрисовка Декали в точке попадания
		if (OtherComp)
		{
			UMaterialInterface* myMaterialInterface = nullptr;

			if (ProjectileSetting.HitDecals.Contains(mySurfaceType))
			{
				myMaterialInterface = ProjectileSetting.HitDecals[mySurfaceType];
			}
			else if (ProjectileSetting.HitDecals.Contains(EPhysicalSurface::SurfaceType_Default))
			{
				myMaterialInterface = ProjectileSetting.HitDecals[EPhysicalSurface::SurfaceType_Default];
			}

			if (myMaterialInterface)
			{
				SpawnHitDecal_Multicast(myMaterialInterface, OtherComp, Hit);
			}
		}
		//---

		// Воспроизведение эффекта (FX)
		UParticleSystem* myParticle = nullptr;

		if (ProjectileSetting.HitFXs.Contains(mySurfaceType))
		{
			myParticle = ProjectileSetting.HitFXs[mySurfaceType];
		}
		else if (ProjectileSetting.HitFXs.Contains(EPhysicalSurface::SurfaceType_Default))
		{
			myParticle = ProjectileSetting.HitFXs[EPhysicalSurface::SurfaceType_Default];
		}

		if (myParticle)
		{
			SpawnFX(myParticle, Hit);
		}
		//---

		// Воспроизведение звука
		USoundBase* mySound = nullptr;

		if (ProjectileSetting.HitSound.Contains(mySurfaceType))
		{
			mySound = ProjectileSetting.HitSound[mySurfaceType];
		}
		else if (ProjectileSetting.HitSound.Contains(EPhysicalSurface::SurfaceType_Default))
		{
			mySound = ProjectileSetting.HitSound[EPhysicalSurface::SurfaceType_Default];
		}

		if (mySound)
		{
			SpawnSound(mySound, Hit);
		}
		//---

		// Добавление эффекта по типу поверхности
		UTypes::AddEffectBySurfaceType(
			Hit.GetActor(),
			Hit.BoneName,
			ProjectileSetting.Effect,
			mySurfaceType);
		//---
	}

	UGameplayStatics::ApplyPointDamage(
		OtherActor,
		ProjectileSetting.ProjectileDamage,
		Hit.TraceStart,
		Hit,
		GetInstigatorController(),
		this,
		NULL);

	UAISense_Damage::ReportDamageEvent(		// todo shotgun trace, grenade
		GetWorld(),
		Hit.GetActor(),
		GetInstigator(),
		ProjectileSetting.ProjectileDamage,
		Hit.Location,
		Hit.Location);

	ImpactProjectile();
}

void AProjectileDefault::BulletCollisionSphereBeginOverlap(
	UPrimitiveComponent* OverlapedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult& SweepResult)
{
}

void AProjectileDefault::BulletCollisionSphereEndOverlap(
	UPrimitiveComponent* OverlapedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex)
{
}

// Влияние снаряда
void AProjectileDefault::ImpactProjectile()
{
	this->Destroy();
}
// ----------------------------------------------------------------------------------------------------



/* ---   Adapter to NetWork   --- */
void AProjectileDefault::SpawnFX(UParticleSystem* FxTemplate, const FHitResult& HitResult)
{
	SpawnFX_Multicast(
		FxTemplate,
		FTransform(
			HitResult.ImpactNormal.Rotation() + FRotator(-90, 0, 0),
			HitResult.ImpactPoint,
			FVector(1.0f)));
}

void AProjectileDefault::SpawnFX(UParticleSystem* FxTemplate, const FTransform& Transform)
{
	SpawnFX_Multicast(FxTemplate, Transform);
}

void AProjectileDefault::SpawnSound(USoundBase* HitSound, const FHitResult& HitResult)
{
	SpawnSound_Multicast(HitSound, HitResult.ImpactPoint);
}

void AProjectileDefault::SpawnSound(USoundBase* HitSound, const FVector& Location)
{
	SpawnSound_Multicast(HitSound, Location);
}
// ----------------------------------------------------------------------------------------------------



/* ---   NetWork   --- */
void AProjectileDefault::InitVisualMeshProjectile_Multicast_Implementation(UStaticMesh* newMesh, FTransform MeshRelative)
{
	BulletMesh->SetStaticMesh(newMesh);
	BulletMesh->SetRelativeTransform(MeshRelative);
}

void AProjectileDefault::InitVisualTrailProjectile_Multicast_Implementation(UParticleSystem* newTemplate, FTransform TemplateRelative)
{
	BulletFX->SetTemplate(newTemplate);
	BulletFX->SetRelativeTransform(TemplateRelative);
}

void AProjectileDefault::SpawnHitDecal_Multicast_Implementation(UMaterialInterface* DecalMaterial, UPrimitiveComponent* OtherComp, FHitResult HitResult)
{
	UGameplayStatics::SpawnDecalAttached(
		DecalMaterial,
		FVector(20.0f),
		OtherComp,
		NAME_None,
		HitResult.ImpactPoint,
		HitResult.ImpactNormal.Rotation(),
		EAttachLocation::KeepWorldPosition,
		10.0f);
}

void AProjectileDefault::SpawnFX_Multicast_Implementation(UParticleSystem* FxTemplate, const FTransform& Transform)
{
	UGameplayStatics::SpawnEmitterAtLocation(
		GetWorld(),
		FxTemplate,
		Transform);
}

void AProjectileDefault::SpawnSound_Multicast_Implementation(USoundBase* HitSound, const FVector& Location)
{
	UGameplayStatics::PlaySoundAtLocation(
		GetWorld(),
		HitSound,
		Location);
}
// ----------------------------------------------------------------------------------------------------
