﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "Components/SphereComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"

#include "OnePsycho/FuncLibrary/Types.h"
#include "ProjectileDefault.generated.h"

UCLASS()
class ONEPSYCHO_API AProjectileDefault : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AProjectileDefault();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UStaticMeshComponent* BulletMesh = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class USphereComponent* BulletCollisionSphere = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UProjectileMovementComponent* BulletProjectileMovement = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UParticleSystemComponent* BulletFX = nullptr;

	UPROPERTY(BlueprintReadOnly)
	FProjectileInfo ProjectileSetting;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	/* ---   Projectile Info   --- */
	UFUNCTION(BlueprintCallable)
	void InitProjectile(FProjectileInfo InitParam);

	virtual void LifeSpan(FProjectileInfo InitParam);
	// ----------------------------------------------------------------------------------------------------



	/* ---   Hit   --- */
	UFUNCTION()
	virtual void BulletCollisionSphereHit(
		class UPrimitiveComponent* HitComp,
		AActor* OtherActor,
		UPrimitiveComponent* OtherComp,
		FVector NormalImpulse,
		const FHitResult& Hit);

	UFUNCTION()
	virtual void BulletCollisionSphereBeginOverlap(
		UPrimitiveComponent* OverlapedComponent,
		AActor* OtherActor,
		UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex,
		bool bFromSweep,
		const FHitResult& SweepResult);

	UFUNCTION()
	virtual void BulletCollisionSphereEndOverlap(
		UPrimitiveComponent* OverlapedComponent,
		AActor* OtherActor,
		UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex);

	UFUNCTION()
	virtual void ImpactProjectile();
	// ----------------------------------------------------------------------------------------------------



	/* ---   Adapter to NetWork   --- */
	void SpawnFX(UParticleSystem* FxTemplate, const FHitResult& HitResult);
	void SpawnFX(UParticleSystem* FxTemplate, const FTransform& Transform);

	void SpawnSound(USoundBase* HitSound, const FHitResult& HitResult);
	void SpawnSound(USoundBase* HitSound, const FVector& Location);
	// ----------------------------------------------------------------------------------------------------



	/* ---   NetWork   --- */
	UFUNCTION(NetMulticast, Reliable)
	void InitVisualMeshProjectile_Multicast(UStaticMesh* newMesh, FTransform MeshRelative);

	UFUNCTION(NetMulticast, Reliable)
	void InitVisualTrailProjectile_Multicast(UParticleSystem* newTemplate, FTransform TemplateRelative);

	UFUNCTION(NetMulticast, Reliable)
	void SpawnHitDecal_Multicast(UMaterialInterface* DecalMaterial, UPrimitiveComponent* OtherComp, FHitResult HitResult);

	UFUNCTION(NetMulticast, Reliable)
	void SpawnFX_Multicast(UParticleSystem* FxTemplate, const FTransform& Transform);

	UFUNCTION(NetMulticast, Reliable)
	void SpawnSound_Multicast(USoundBase* HitSound, const FVector& Location);
	// ----------------------------------------------------------------------------------------------------
};