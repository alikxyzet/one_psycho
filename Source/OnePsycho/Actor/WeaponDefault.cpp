﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponDefault.h"
#include "Engine/StaticMeshActor.h"
#include "Net/UnrealNetwork.h"

bool bShowDebug = false;
FAutoConsoleVariableRef CVARWeaponShow(
	TEXT("OP.Weapon.Debug"),
	bShowDebug,
	TEXT("Draw Debug for Weapon"),
	ECVF_Cheat);

// Sets default values
AWeaponDefault::AWeaponDefault()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// NetWork
	SetReplicates(true);

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
	SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
	SkeletalMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMeshWeapon->SetupAttachment(RootComponent);

	StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	StaticMeshWeapon->SetGenerateOverlapEvents(false);
	StaticMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	StaticMeshWeapon->SetupAttachment(RootComponent);

	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("Shoot Location"));
	ShootLocation->SetupAttachment(RootComponent);

	ClipDropLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("Clip Drop Location"));
	ClipDropLocation->SetupAttachment(RootComponent);

	SleeveBulletDropLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("Sleeve Bullet Drop Location"));
	SleeveBulletDropLocation->SetupAttachment(RootComponent);
}



/* ---   Basic   --- */

// Called every frame
void AWeaponDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FireTick(DeltaTime);
	ReloadTick(DeltaTime);
	DispersionTick(DeltaTime);

}

// Called when the game starts or when spawned
void AWeaponDefault::BeginPlay()
{
	Super::BeginPlay();

	/* Warning!!   Инициализация:
	* Удаление неиспользуемых компонентов, зависимых от вводимых данных из вне,
	* производить ПОСЛЕ загрузки параметров -- реализованно через "Character.cpp".
	* Ибо сперва вызывается "BeginPlay()" в "WeaponDefault",
	* и только потом происходит установка настроек через "Character.cpp",
	* что удаляет компоненты ДО загрузки используемых ими параметров
	*/
	// Удаление компонентов, зависимых от "WeaponDefault_N"
	WeaponInit();
}
// ----------------------------------------------------------------------------------------------------



/* ---   Event   --- */

// 
void AWeaponDefault::WeaponReloadStart(UAnimMontage* Anim)
{
	WeaponReloadStart_BP(Anim);
}

void AWeaponDefault::WeaponReloadStart_BP_Implementation(UAnimMontage* Anim)
{
	// in BP
}

// 
void AWeaponDefault::WeaponReloadEnd()
{
	WeaponReloadEnd_BP();
}

void AWeaponDefault::WeaponReloadEnd_BP_Implementation()
{
	// in BP
}

// 
void AWeaponDefault::WeaponFireStart(UAnimMontage* Anim)
{
	WeaponFireStart_BP(Anim);
}

void AWeaponDefault::WeaponFireStart_BP_Implementation(UAnimMontage* Anim)
{
	// in BP
}
// ----------------------------------------------------------------------------------------------------



/* ---   Weapon Info   --- */
/* 1.	Удаление компонентов, зависимых от "WeaponDefault_N"
*  2.	Установка начальных данных
*/
void AWeaponDefault::WeaponInit()
{
	if (SkeletalMeshWeapon && !(SkeletalMeshWeapon->SkeletalMesh))
		SkeletalMeshWeapon->DestroyComponent(true);

	if (StaticMeshWeapon && !(StaticMeshWeapon->GetStaticMesh()))
		StaticMeshWeapon->DestroyComponent(true);
}

// Удаление компонентов, зависимых от вводимых данных из вне
void AWeaponDefault::DestroyUnusedComponents()
{
	if (ClipDropLocation)
	{
		if (!(WeaponSetting.ClipDropMesh.DropMesh))
		{
			ClipDropLocation->DestroyComponent(true);
		}
	}

	if (SleeveBulletDropLocation)
	{
		if (!(WeaponSetting.SleeveBullets.DropMesh))
		{
			SleeveBulletDropLocation->DestroyComponent(true);
		}
	}
}



void AWeaponDefault::UpdateStateWeapon_OnServer_Implementation(EMovementState NewMovementState)
{
	BlockFire = false;

	switch (NewMovementState)
	{
	case EMovementState::Aim_State:
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Aim_State.AimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Aim_State.AimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Aim_State.Recoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_State.Reduction;
		break;
	case EMovementState::AimWalk_State:
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.AimWalk_State.AimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.AimWalk_State.AimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.AimWalk_State.Recoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.AimWalk_State.Reduction;
		break;
	case EMovementState::Walk_State:
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Walk_State.AimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Walk_State.AimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Walk_State.Recoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Walk_State.Reduction;
		break;
	case EMovementState::Run_State:
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Run_State.AimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Run_State.AimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Run_State.Recoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Run_State.Reduction;
		break;
	case EMovementState::SprintRun_State:
		BlockFire = true;
		SetWeaponStateFire_OnServer(false);
		break;
	default:
		break;
	}



	//ChangeDispersion();
}

void AWeaponDefault::ChangeDispersion()
{
}

int32 AWeaponDefault::GetWeaponRound() const
{
	return AdditionalWeaponInfo.Round;
}
// ----------------------------------------------------------------------------------------------------



/* ---   Projectile   --- */
FProjectileInfo AWeaponDefault::GetProjectile() const
{
	return WeaponSetting.ProjectileSetting;
}
// ----------------------------------------------------------------------------------------------------



/* ---   Fire   --- */
void AWeaponDefault::FireTick(float DeltaTime)
{
	if (bWeaponFiring && GetWeaponRound() > 0 && !bWeaponReloading)
		if (FireTimer < 0.f)
		{
			Fire();
		}
		else
			FireTimer -= DeltaTime;
}

// Режим огня (На сервере)
void AWeaponDefault::Fire()
{
	// On Server by Weapon Fire bool

	// Выброс гильзы
	InitDropMesh(WeaponSetting.SleeveBullets, *SleeveBulletDropLocation);

	// Получение скорости стрельбы
	FireTimer = WeaponSetting.RateOfFire;
	// Уменьшение обоймы
	AdditionalWeaponInfo.Round--;
	// Увеличение дисперсии
	ChangeDispersionByShot();

	// Вывод звука и эффекта
	SpawnSoundAndFX_Multicast(
		WeaponSetting.EffectFireWeapon,
		WeaponSetting.SoundFireWeapon,
		ShootLocation->GetComponentTransform());

	// Запуск анимации стрельбы: Персонаж
	if (bWeaponAiming)
	{
		if (WeaponSetting.CharacterAnim.AimedFire)
			OnWeaponFireStart.Broadcast(WeaponSetting.CharacterAnim.AimedFire);
	}
	else
	{
		if (WeaponSetting.CharacterAnim.Fire)
			OnWeaponFireStart.Broadcast(WeaponSetting.CharacterAnim.Fire);
	}

	// Запуск анимации стрельбы: Оружие
	if (WeaponSetting.WeaponAnim.Fire)
	{
		AnimWeaponStart_Multicast(WeaponSetting.WeaponAnim.Fire);
	}

	if (ShootLocation)
	{
		FVector SpawnLocation = ShootLocation->GetComponentLocation();
		FRotator SpawnRotation = ShootLocation->GetComponentRotation();
		FProjectileInfo ProjectileInfo = GetProjectile();

		FVector EndLocation;

		int8 NumberProjectile = GetNumberProjectileByShot();

		for (int8 i = 0; i < NumberProjectile; i++)
		{
			// Получение конечной точки с дисперсией
			EndLocation = GetFireEndLocation();

			// Тип стрельбы: Баллистика или HitScan
			if (ProjectileInfo.Projectile && ProjectileInfo.TypeFire != ETypeFire::HitScan)
			{
				// Перенаправление "пули" в направлении курсора (не зависит от ротации "оружия")
				FVector Dir = EndLocation - SpawnLocation;
				Dir.Normalize();
				FMatrix myMatrix(Dir, FVector(0, 0, 0), FVector(0, 0, 0), FVector::ZeroVector);
				SpawnRotation = myMatrix.Rotator();

				// Создание "пули" с параметрами

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = GetOwner();
				SpawnParams.Instigator = GetInstigator();

				AProjectileDefault* myProjectile = Cast<AProjectileDefault>(GetWorld()->SpawnActor(ProjectileInfo.Projectile, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myProjectile)
				{
					myProjectile->InitProjectile(WeaponSetting.ProjectileSetting);
				}

			}
			else if (ProjectileInfo.TypeFire == ETypeFire::HitScan)
			{
				// ToDo Multicast trace FX

				FHitResult Hit;
				TArray<AActor*> Actors;

				UKismetSystemLibrary::LineTraceSingle(
					GetWorld(),
					SpawnLocation,
					EndLocation * WeaponSetting.DistanceTrace,
					ETraceTypeQuery::TraceTypeQuery4,
					false,
					Actors,
					EDrawDebugTrace::ForDuration,
					Hit,
					true,
					FLinearColor::Red,
					FLinearColor::Green,
					5.f);

				if (bShowDebug)
					DrawDebugLine(
						GetWorld(),
						SpawnLocation,
						SpawnLocation + ShootLocation->GetForwardVector() * WeaponSetting.DistanceTrace,
						FColor::Black,
						false,
						5.f,
						(uint8)'\000',
						0.5f);

				if (Hit.GetActor() && Hit.PhysMaterial.IsValid())
				{
					// Реакция на Попадание
					ReactionToHit(
						Hit.GetComponent(),
						Hit,
						WeaponSetting.ProjectileSetting);

					UGameplayStatics::ApplyPointDamage(
						Hit.GetActor(),
						WeaponSetting.ProjectileSetting.ProjectileDamage,
						Hit.TraceStart,
						Hit,
						GetInstigatorController(),
						this,
						NULL);
				}
			}
		}
	}

	if (GetWeaponRound() <= 0 && !bWeaponReloading)
	{
		if (CheckCanWeaponReload())
			InitReload();
	}
}

// Проверка: Оружие в режиме стрельбы
bool AWeaponDefault::ChecWeaponCanFire() const
{
	return !BlockFire;
}

// Установка: Оружие в режиме стрельбы
void AWeaponDefault::SetWeaponStateFire_OnServer_Implementation(bool bIsFire)
{
	if (ChecWeaponCanFire())
		bWeaponFiring = bIsFire;
	else
		bWeaponFiring = false;
}

// Привязка к bool: Оружие в режиме прицеливания
void AWeaponDefault::SetWeaponStateAimed(bool bIsAimed)
{
	bWeaponAiming = bIsAimed;
}

// Получить конечную локацию огня (с дисперсией)
FVector AWeaponDefault::GetFireEndLocation() const
{
	bool bShootDirection = false;
	FVector EndLocation = FVector(0);

	FVector tmpV = ShootLocation->GetComponentLocation() - ShootEndLocation;

	if (tmpV.Size() > SizeVectorToChangeShootDirectionLogic)
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot(tmpV.GetSafeNormal()) * -20000.0f;
	else
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot(ShootLocation->GetForwardVector()) * 20000.0f;

	if (bShowDebug)
	{
		FVector DirectionForDebag = FVector(0);
		if (tmpV.Size() > SizeVectorToChangeShootDirectionLogic)
			DirectionForDebag = -tmpV;
		else
			DirectionForDebag = ShootLocation->GetForwardVector();

		// Конус дисперсии
		DrawDebugCone(GetWorld(),					// Мир
			ShootLocation->GetComponentLocation(),	// Начало
			DirectionForDebag,		// Направление
			WeaponSetting.DistanceTrace,			// Длина
			GetCurrentDispersion() * PI / 180.f,	// Угловая Ширина
			GetCurrentDispersion() * PI / 180.f,	// Угловая Высота
			32,						// Количество линий
			FColor::Emerald,		// Цвет
			false, 					// Постоянство линий
			0.1f, 					// Время жизни
			(uint8)'\000', 			// Уровень приоритета
			1.0f);

		// Направление оружия
		DrawDebugLine(GetWorld(),	// Мир
			ShootLocation->GetComponentLocation(),		// Начало линии
			ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector() * 500.0f,		// Конец линии
			FColor::Cyan,			// Цвет
			false,					// Постоянство линии
			5.f,					// Время жизни
			(uint8)'\000',			// Уровень приоритета
			0.5f);					// Толщина

		// Направление снаряда куда должен лететь
		DrawDebugLine(GetWorld(),	// Мир
			ShootLocation->GetComponentLocation(),		// Начало линии
			ShootEndLocation,		// Конец линии
			FColor::Red,			// Цвет
			false,					// Постоянство линии
			5.f,					// Время жизни
			(uint8)'\000',			// Уровень приоритета
			0.5f);					// Толщина

		// Направление текущего снаряда
		DrawDebugLine(GetWorld(),	// Мир
			ShootLocation->GetComponentLocation(),		// Начало линии
			EndLocation,			// Конец линии								
			FColor::Black, 			// Цвет
			false, 					// Постоянство линии
			5.f, 					// Время жизни
			(uint8)'\000', 			// Уровень приоритета
			0.5f);					// Толщина
	}

	return EndLocation;
}

// Реакция на попадание
void AWeaponDefault::ReactionToHit(
	UPrimitiveComponent* OtherComp,
	const FHitResult& Hit,
	const FProjectileInfo& ProjectileSetting)
{
	EPhysicalSurface mySurfaceType = UGameplayStatics::GetSurfaceType(Hit);

	// Отрисовка Декали в точке попадания
	if (OtherComp)
	{
		UMaterialInterface* myMaterialInterface = nullptr;

		if (ProjectileSetting.HitDecals.Contains(mySurfaceType))
		{
			myMaterialInterface = ProjectileSetting.HitDecals[mySurfaceType];
		}
		else if (ProjectileSetting.HitDecals.Contains(EPhysicalSurface::SurfaceType_Default))
		{
			myMaterialInterface = ProjectileSetting.HitDecals[EPhysicalSurface::SurfaceType_Default];
		}

		if (myMaterialInterface)
		{
			SpawnHitDecal_Multicast(myMaterialInterface, OtherComp, Hit);
		}
	}
	//---

	// Получение эффекта (FX)
	UParticleSystem* myParticle = nullptr;

	if (ProjectileSetting.HitFXs.Contains(mySurfaceType))
	{
		myParticle = ProjectileSetting.HitFXs[mySurfaceType];
	}
	else if (ProjectileSetting.HitFXs.Contains(EPhysicalSurface::SurfaceType_Default))
	{
		myParticle = ProjectileSetting.HitFXs[EPhysicalSurface::SurfaceType_Default];
	}
	//---

	// Получение звука
	USoundBase* mySound = nullptr;

	if (ProjectileSetting.HitSound.Contains(mySurfaceType))
	{
		mySound = ProjectileSetting.HitSound[mySurfaceType];
	}
	else if (ProjectileSetting.HitSound.Contains(EPhysicalSurface::SurfaceType_Default))
	{
		mySound = ProjectileSetting.HitSound[EPhysicalSurface::SurfaceType_Default];
	}
	//---

	// Воспроизведение звука и эффекта (FX)
	SpawnSoundAndFX_Multicast(
		myParticle,
		mySound,
		FTransform(
			Hit.ImpactNormal.Rotation() + FRotator(-90, 0, 0),
			Hit.ImpactPoint,
			FVector(1.0f)));
	//---

	// Добавление эффекта по типу поверхности
	UTypes::AddEffectBySurfaceType(
		Hit.GetActor(),
		Hit.BoneName,
		ProjectileSetting.Effect,
		mySurfaceType);
	//---
}

int8 AWeaponDefault::GetNumberProjectileByShot() const
{
	return WeaponSetting.NumberProjectileByShot;
}

void AWeaponDefault::UpdateWeaponByCharacterMovementState_OnServer_Implementation(FVector NewShootEndLocation, bool bNewShouldReduceDispersion)
{
	ShootEndLocation = NewShootEndLocation;
	bShouldReduceDispersion = bNewShouldReduceDispersion;
}

void AWeaponDefault::AnimWeaponStart_Multicast_Implementation(UAnimMontage* Anim)
{
	// Запуск анимации Оружия
	if (Anim
		&& SkeletalMeshWeapon
		&& SkeletalMeshWeapon->GetAnimInstance())//Bad Code? maybe best way init local variable or in func
	{
		SkeletalMeshWeapon->GetAnimInstance()->Montage_Play(Anim);
	}
}

void AWeaponDefault::ShellDropFire_Multicast_Implementation(
	UStaticMesh* DropMesh,
	FTransform Offset,
	FVector DropImpulseDirection,
	float LifeTimeMesh)
{
	FActorSpawnParameters SpawnParams;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnParams.Owner = GetOwner();

	// Создание компонента выброса с параметрами
	AStaticMeshActor* NewActor = GetWorld()->SpawnActor<AStaticMeshActor>(AStaticMeshActor::StaticClass(), Offset, SpawnParams);
	// Дополнительные параметры компонента
	if (NewActor && NewActor->GetStaticMeshComponent())
	{
		// Установить в группу колизии
		NewActor->GetStaticMeshComponent()->SetCollisionProfileName(TEXT("WeaponsObjects"));

		NewActor->SetActorTickEnabled(false);

		// Группа обьекта: Подвижный
		NewActor->GetStaticMeshComponent()->Mobility = EComponentMobility::Movable;
		// Включить симуляцию физики
		NewActor->GetStaticMeshComponent()->SetSimulatePhysics(true);
		// Установить StaticMesh
		NewActor->GetStaticMeshComponent()->SetStaticMesh(DropMesh);

		// Задать импульс
		NewActor->GetStaticMeshComponent()->AddImpulse(DropImpulseDirection);

		// Задать время жизни
		NewActor->SetLifeSpan(LifeTimeMesh);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("AWeaponDefault::ShellDropFire_Multicast - NewActor or NewActor->GetStaticMeshComponent() -NULL"));
	}
}

// Вывод звука и эффекта
void AWeaponDefault::SpawnSoundAndFX_Multicast_Implementation(
	UParticleSystem* in_FX,
	USoundBase* in_Sound,
	const FTransform& in_Transform)
{
	if (in_FX)
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), in_FX, in_Transform);

	if (in_Sound)
		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), in_Sound, in_Transform.GetLocation());
}
// ----------------------------------------------------------------------------------------------------



/* ---   Reload   --- */
// Tick Func
void AWeaponDefault::ReloadTick(float DeltaTime)
{
	if (bWeaponReloading)
	{
		if (ReloadTimer < 0.f)
		{
			FinishReload();
		}
		else
		{
			ReloadTimer -= DeltaTime;
		}
	}
}

// Запуск процесса перезарядки
void AWeaponDefault::InitReload()
{
	if (!bWeaponReloading)
	{
		bWeaponReloading = true;

		ReloadTimer = WeaponSetting.ReloadTime;

		// Запуск анимации перезарядки
		if (bWeaponAiming)
		{
			if (WeaponSetting.CharacterAnim.ReloadWhenAiming)
			{
				OnWeaponReloadStart.Broadcast(WeaponSetting.CharacterAnim.ReloadWhenAiming);

				if (WeaponSetting.WeaponAnim.ReloadWhenAiming)
					AnimWeaponStart_Multicast(WeaponSetting.WeaponAnim.ReloadWhenAiming);
			}
			else if (WeaponSetting.CharacterAnim.ReloadFromHip)
			{
				OnWeaponReloadStart.Broadcast(WeaponSetting.CharacterAnim.ReloadFromHip);

				if (WeaponSetting.WeaponAnim.ReloadFromHip)
					AnimWeaponStart_Multicast(WeaponSetting.WeaponAnim.ReloadFromHip);
			}
		}
		else
		{
			if (WeaponSetting.CharacterAnim.ReloadFromHip)
				OnWeaponReloadStart.Broadcast(WeaponSetting.CharacterAnim.ReloadFromHip);

			if (WeaponSetting.WeaponAnim.ReloadFromHip)
				AnimWeaponStart_Multicast(WeaponSetting.WeaponAnim.ReloadFromHip);
		}
	}
}

// Завершение перезарядки
void AWeaponDefault::FinishReload()
{
	bWeaponReloading = false;

	int8 AviableAmmoFromInventory = GetAviableAmmoForReload();
	int8 AmmoNeedTakeFromInv;
	int8 NeedToReload = WeaponSetting.MaxRound - AdditionalWeaponInfo.Round;

	if (NeedToReload > AviableAmmoFromInventory)
	{
		AdditionalWeaponInfo.Round += AviableAmmoFromInventory;
		AmmoNeedTakeFromInv = AviableAmmoFromInventory;
	}
	else
	{
		AdditionalWeaponInfo.Round += NeedToReload;
		AmmoNeedTakeFromInv = NeedToReload;
	}

	OnWeaponReloadEnd.Broadcast(true, -AmmoNeedTakeFromInv);


	// Останов анимации перезарядки
	WeaponReloadEnd();

	FDropImpulse ClipDropImpulse(ClipDropLocation->GetComponentTransform());

	InitDropMesh(WeaponSetting.ClipDropMesh, *ClipDropLocation);
	// ------------------------------
}


void AWeaponDefault::CancelReload()
{
	bWeaponReloading = false;
	if (SkeletalMeshWeapon && SkeletalMeshWeapon->GetAnimInstance())
		SkeletalMeshWeapon->GetAnimInstance()->StopAllMontages(0.15f);

	OnWeaponReloadEnd.Broadcast(false, 0);
}

bool AWeaponDefault::CheckCanWeaponReload()
{
	bool result = true;
	if (GetOwner())
	{
		UOnePsychoInventoryComponent* MyInv = Cast<UOnePsychoInventoryComponent>(GetOwner()->GetComponentByClass(UOnePsychoInventoryComponent::StaticClass()));
		if (MyInv)
		{
			int8 AviableAmmoForWeapon;
			if (!MyInv->CheckAmmoForWeapon(WeaponSetting.WeaponType, AviableAmmoForWeapon))
			{
				result = false;
			}
		}
	}

	return result;
}


int8 AWeaponDefault::GetAviableAmmoForReload()
{
	int8 AviableAmmoForWeapon = WeaponSetting.MaxRound;
	if (GetOwner())
	{
		UOnePsychoInventoryComponent* MyInv = Cast<UOnePsychoInventoryComponent>(GetOwner()->GetComponentByClass(UOnePsychoInventoryComponent::StaticClass()));
		if (MyInv)
		{
			if (MyInv->CheckAmmoForWeapon(WeaponSetting.WeaponType, AviableAmmoForWeapon))
			{
				// Сам на себя??? Надо проверить целесообразность и/или перестроить
				AviableAmmoForWeapon = AviableAmmoForWeapon;
			}
		}
	}
	return AviableAmmoForWeapon;
}
// ----------------------------------------------------------------------------------------------------



/* ---   Dispersion   --- */
// Tick Func
void AWeaponDefault::DispersionTick(float DeltaTime)
{
	if (!bWeaponReloading)
	{
		if (!bWeaponFiring)
		{
			if (bShouldReduceDispersion)
				CurrentDispersion -= CurrentDispersionReduction;
			else
				CurrentDispersion += CurrentDispersionReduction;
		}

		// Ограничение по Мин и Макс
		if (CurrentDispersion < CurrentDispersionMin)
			CurrentDispersion = CurrentDispersionMin;
		else if (CurrentDispersion > CurrentDispersionMax)
			CurrentDispersion = CurrentDispersionMax;
	}
}

void AWeaponDefault::ChangeDispersionByShot()
{
	CurrentDispersion += CurrentDispersionRecoil;
}

float AWeaponDefault::GetCurrentDispersion() const
{
	return CurrentDispersion;
}

FVector AWeaponDefault::ApplyDispersionToShoot(FVector DirectionShoot) const
{
	return FMath::VRandCone(DirectionShoot, GetCurrentDispersion() * PI / 180.f);
}
// ----------------------------------------------------------------------------------------------------



/* ---   Drop Mesh   --- */
void AWeaponDefault::InitDropMesh(FDropMeshInfo& DropMeshInfo, UArrowComponent& ArrowComponent)
{
	if (DropMeshInfo.DropMesh)
	{
		// Получение настроек импульса для SleeveBulletDropLocation
		FDropImpulse DropImpulse;
		if (DropMeshInfo.bCustomDropImpulse)
		{
			ArrowComponent.SetRelativeLocation(DropMeshInfo.DropMeshImpulse.GetLocation());
			DropImpulse.SetLocation(ArrowComponent.GetComponentLocation());
			DropImpulse.AddDirection(ArrowComponent.GetComponentRotation());
			DropImpulse.SetRotation(DropMeshInfo.DropMeshImpulse.GetRotation());
		}
		else
		{
			DropImpulse = FDropImpulse(ArrowComponent.GetComponentTransform(), FRotator(90, 0, 0));
		}

		// Получение настроек компонента для выброса
		FVector SpawnLocation = DropImpulse.GetLocation();
		FRotator SpawnRotation = this->GetActorRotation() + DropImpulse.GetRotation();

		// Создание компонента выброса с параметрами
		ShellDropFire_Multicast(
			DropMeshInfo.DropMesh,
			DropImpulse.GetTransform(),
			DropImpulse.GetDirectionNormalize(DropMeshInfo.PowerImpulse),
			DropMeshInfo.DropMeshLifeTime);
	}
}
// ----------------------------------------------------------------------------------------------------



/* ---   NetWork: Hit   --- */
void AWeaponDefault::SpawnHitDecal_Multicast_Implementation(UMaterialInterface* DecalMaterial, UPrimitiveComponent* OtherComp, FHitResult HitResult)
{
	UGameplayStatics::SpawnDecalAttached(
		DecalMaterial,
		FVector(20.0f),
		OtherComp,
		NAME_None,
		HitResult.ImpactPoint,
		HitResult.ImpactNormal.Rotation(),
		EAttachLocation::KeepWorldPosition,
		10.0f);
}
// ----------------------------------------------------------------------------------------------------



/* ---   NET   --- */
void AWeaponDefault::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AWeaponDefault, AdditionalWeaponInfo);
	DOREPLIFETIME(AWeaponDefault, ShootEndLocation);
	//DOREPLIFETIME(AWeaponDefault, bShouldReduceDispersion);
}
// ----------------------------------------------------------------------------------------------------


/* ---   Debug   --- */



// ----------------------------------------------------------------------------------------------------