﻿// Copyright Epic Games, Inc. All Rights Reserved.

#include "OnePsychoCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Engine/World.h"

#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Net/UnrealNetwork.h"

#include "OnePsycho/Game/OnePsychoGameInstance.h"
#include "OnePsycho/Actor/ProjectileDefault.h"
#include "OnePsycho/OnePsycho.h"

bool bDebugImmunity = false;
FAutoConsoleVariableRef CVARImmunity(
	TEXT("OP.Character.Immunity"),
	bDebugImmunity,
	TEXT("The Character is Invulnerable"),
	ECVF_Cheat);

AOnePsychoCharacter::AOnePsychoCharacter()
{
	// Установка размера для капсулы игрока
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Не вращать Character в направление камеры
	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Настройка движения:
	// Configure character movement:
	// Поворот Character в направлении движения: ВЫКЛ
	GetCharacterMovement()->bOrientRotationToMovement = false;
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	/* ---   Иниц. доп. компонентов   --- */
	// Inventory
	InventoryComponent = CreateDefaultSubobject<UOnePsychoInventoryComponent>(TEXT("InventoryComponent"));

	if (InventoryComponent)
	{
		// Делегат: получение информации
		InventoryComponent->OnSwitchWeapon.AddDynamic(this, &AOnePsychoCharacter::InitWeapon);
	}

	// Health
	CharHealthComponent = CreateDefaultSubobject<UOPCharacterHealthComponent>(TEXT("HealthComponent"));

	if (CharHealthComponent)
	{
		CharHealthComponent->OnDead.AddDynamic(this, &AOnePsychoCharacter::CharDead);
	}
	// ----------------------------------------------------------------------------------------------------

	// NetWork
	SetReplicates(true);
}



/* ---   Basic   --- */

void AOnePsychoCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	CursorTick();
	MovementTick();
}

void AOnePsychoCharacter::BeginPlay()
{
	Super::BeginPlay();

	if (GetWorld() && GetWorld()->GetNetMode() != NM_DedicatedServer)
	{
		if (CursorMaterial && (GetLocalRole() == ROLE_AutonomousProxy || GetLocalRole() == ROLE_Authority))
		{
			CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
		}
	}
}

// Реакции на событие
void AOnePsychoCharacter::SetupPlayerInputComponent(UInputComponent* NewInputComponent)
{
	Super::SetupPlayerInputComponent(NewInputComponent);

	NewInputComponent->BindAxis(TEXT("MoveForward"), this, &AOnePsychoCharacter::InputAxisX);
	NewInputComponent->BindAxis(TEXT("MoveRight"), this, &AOnePsychoCharacter::InputAxisY);

	NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Pressed, this, &AOnePsychoCharacter::InputAttackPressed);
	NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Released, this, &AOnePsychoCharacter::InputAttackReleaset);
	NewInputComponent->BindAction(TEXT("ReloadEvent"), EInputEvent::IE_Released, this, &AOnePsychoCharacter::TryReloadWeapon);

	NewInputComponent->BindAction(TEXT("AimingEvent"), EInputEvent::IE_Pressed, this, &AOnePsychoCharacter::InputAimingPressed);
	NewInputComponent->BindAction(TEXT("AimingEvent"), EInputEvent::IE_Released, this, &AOnePsychoCharacter::InputAimingReleaset);

	NewInputComponent->BindAction(TEXT("SwitchNextWeapon"), EInputEvent::IE_Pressed, this, &AOnePsychoCharacter::TrySwicthNextWeapon);
	NewInputComponent->BindAction(TEXT("SwitchPreviousWeapon"), EInputEvent::IE_Pressed, this, &AOnePsychoCharacter::TrySwitchPreviousWeapon);

	NewInputComponent->BindAction(TEXT("AblityAction"), EInputEvent::IE_Pressed, this, &AOnePsychoCharacter::TryAbilityEnabled);
}
// ----------------------------------------------------------------------------------------------------



/* ---   Event   --- */

// 
void AOnePsychoCharacter::WeaponReloadStart(UAnimMontage* Anim)
{
	WeaponReloadStart_BP(Anim);
}

void AOnePsychoCharacter::WeaponReloadStart_BP_Implementation(UAnimMontage* Anim)
{
	// in BP
}

// 
void AOnePsychoCharacter::WeaponReloadEnd(bool bIsSuccess, int32 AmmoTake)
{
	if (InventoryComponent && CurrentWeapon)
	{
		InventoryComponent->AmmoSlotChangeValue(CurrentWeapon->WeaponSetting.WeaponType, AmmoTake);
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->AdditionalWeaponInfo);
	}

	WeaponReloadEnd_BP(bIsSuccess);
}

void AOnePsychoCharacter::WeaponReloadEnd_BP_Implementation(bool bIsSuccess)
{
	// in BP
}

// 
void AOnePsychoCharacter::WeaponFireStart(UAnimMontage* Anim)
{
	if (InventoryComponent && CurrentWeapon)
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->AdditionalWeaponInfo);
	WeaponFireStart_BP(Anim);
}

void AOnePsychoCharacter::WeaponFireStart_BP_Implementation(UAnimMontage* Anim)
{
	// in BP
}

void AOnePsychoCharacter::CharDead_BP_Implementation()
{
	// in BP
}
// ----------------------------------------------------------------------------------------------------



/* ---   Camera   --- */
// Сдвиг цели (Камеры) на курсор
void AOnePsychoCharacter::OffsetForXY(float OffsetForXY_In, float& TargetOffsetXY)
{
	if (-0.1f < TargetOffsetXY || TargetOffsetXY < 0.1f)	// Фильтр от лишних мизерных вычислений
	{
		float OffsetForXY_Out = 0.0f;

		if (DeadZone != 0)
			if (OffsetForXY_In > DeadZone)
				OffsetForXY_In -= DeadZone;
			else if (OffsetForXY_In < -DeadZone)
				OffsetForXY_In += DeadZone;
			else
				OffsetForXY_In = 0;

		// P-Control for XY
		UTypes::PIDController(
			OffsetForXY_In,
			TargetOffsetXY,
			OffsetForXY_Out,
			ProportionalCoeffForCamera);

		TargetOffsetXY += OffsetForXY_Out;
	}
	else
		TargetOffsetXY = 0; 	// Если значение мизерное, то прировнять к 0
}
// ----------------------------------------------------------------------------------------------------



/* ---   Cursor   --- */
// Tick Func
void AOnePsychoCharacter::CursorTick()
{
	if (CurrentCursor)
	{
		APlayerController* myPC = Cast<APlayerController>(GetController());
		if (myPC && myPC->IsLocalPlayerController())
		{
			FHitResult TraceHitResult;
			myPC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();

			CurrentCursor->SetWorldLocation(TraceHitResult.Location);
			CurrentCursor->SetWorldRotation(CursorR);
		}
	}
}

// Чтение курсора
UDecalComponent* AOnePsychoCharacter::GetCursorToWorld()
{
	return CurrentCursor;
}
// ----------------------------------------------------------------------------------------------------



/* ---   Movement   --- */
// Tick Func
void AOnePsychoCharacter::MovementTick()
{
	if (bIsAlive)
	{
		if (GetController() && GetController()->IsLocalPlayerController())
		{
			AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
			AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);

			FString SEnum = UEnum::GetValueAsString(GetMovementState());
			UE_LOG(LogOnePsycho_Net, Warning, TEXT("Movement state - %s"), *SEnum);

			APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
			if (myController && MovementState != EMovementState::SprintRun_State)
			{
				FHitResult ResultHit;
				bool bResultHit_LocationIsValid = false;

				bResultHit_LocationIsValid = myController->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, ResultHit);

				// Переменные задания смещения камеры
				float OffsetForX_In = 0.f;
				float OffsetForY_In = 0.f;

				// Проверка валидности мыши -- Решает баг с потерей мыши
				if (bResultHit_LocationIsValid)
				{
					OffsetForX_In = (ResultHit.Location.X - CameraBoom->GetComponentLocation().X) * OffsetCoef;
					OffsetForY_In = (ResultHit.Location.Y - CameraBoom->GetComponentLocation().Y) * OffsetCoef;
				}

				// Сдвиг цели (Камеры) на курсор: ось X
				OffsetForXY(OffsetForX_In, CameraBoom->TargetOffset.X);

				// Сдвиг цели (Камеры) на курсор: ось Y
				OffsetForXY(OffsetForY_In, CameraBoom->TargetOffset.Y);

				// Необходимая ротация: 
				float FindRotatorResultYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw;


				SetActorRotation(FQuat(FRotator(0.0f, FindRotatorResultYaw, 0.0f)));
				SetActorRotationByYaw_OnServer(FindRotatorResultYaw);
				// Коррекция направления оружия по оси Z 
				/** (Переделать с float на FVector при необходимости коррекции по другим осям) **/
				if (CurrentWeapon)
				{
					float DisplacementForZ = 0.f;
					bool bIsReduceDispersion = false;

					switch (MovementState)
					{
					case EMovementState::Aim_State:
						DisplacementForZ = 160.f;
						bIsReduceDispersion = true;
						break;
					case EMovementState::AimWalk_State:
						DisplacementForZ = 160.f;
						bIsReduceDispersion = true;
						break;
					case EMovementState::Walk_State:
						DisplacementForZ = 120.f;
						break;
					case EMovementState::Run_State:
						DisplacementForZ = 120.f;
						break;
					case EMovementState::SprintRun_State:
						break;
					default:
						break;
					}

					//CurrentWeapon->ShootEndLocation = ResultHit.Location + FVector(0, 0, DisplacementForZ);

					CurrentWeapon->UpdateWeaponByCharacterMovementState_OnServer(ResultHit.Location + FVector(0, 0, DisplacementForZ), bIsReduceDispersion);
				}
			}
			// Реализация выносливости 
			else if ((AxisX != 0 || AxisY != 0))
			{
				if (SprintRunStamina > MovementSpeedInfo.RunSpeedNormal)
				{
					SprintRunStamina -= StaminaStepDown;
				}
				else
				{
					bSprintRunEnabled = false;
					MovementState = EMovementState::Run_State;
					CharacterUpdate();
				}

				if (SprintRunStamina < MovementSpeedInfo.SprintRunSpeedRun)
				{
					ResSpeed = SprintRunStamina;
					GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
				}

			}
			else
			{
				if (SprintRunStamina < SprintRunStaminaUpperLimit)
				{
					SprintRunStamina += StaminaStepUp;
				}
			}

			// Проверка передвижения персонажа
			//if (CurrentWeapon)
			//	if (FMath::IsNearlyZero(GetVelocity().Size(), 0.5f))
			//		CurrentWeapon->bShouldReduceDispersion = true;
			//	else
			//		CurrentWeapon->bShouldReduceDispersion = false;
		}
	}
}

// Получение направления движения по X
void AOnePsychoCharacter::InputAxisX(float Value)
{
	AxisX = Value;
}

// Получение направления движения по Y
void AOnePsychoCharacter::InputAxisY(float Value)
{
	AxisY = Value;
}

// Изменение статуса передвижения
void AOnePsychoCharacter::ChangeMovementState()
{
	EMovementState NewState = EMovementState::Run_State;
	if (bSprintRunEnabled)
	{
		NewState = EMovementState::SprintRun_State;
		// Поворот Character в направлении движения: ВКЛ
		GetCharacterMovement()->bOrientRotationToMovement = true;
	}
	else
	{
		// Поворот Character в направлении движения: ВЫКЛ
		GetCharacterMovement()->bOrientRotationToMovement = false;

		if (bWalkEnabled && bAimEnabled)
		{
			NewState = EMovementState::AimWalk_State;
		}
		else if (bWalkEnabled && !bAimEnabled)
		{
			NewState = EMovementState::Walk_State;
		}
		else if (!bWalkEnabled && bAimEnabled)
		{
			NewState = EMovementState::Aim_State;
		}
		else if (!bWalkEnabled && !bAimEnabled)
		{
			NewState = EMovementState::Run_State;
		}

	}

	SetMovementState_OnServer(NewState);

	//CharacterUpdate();

	AWeaponDefault* myWeapon = GetCurrentWeapon();
	if (myWeapon)
		myWeapon->UpdateStateWeapon_OnServer(NewState);

}

// Изменение скорости передвижения
void AOnePsychoCharacter::CharacterUpdate()
{
	switch (MovementState)
	{
	case EMovementState::Aim_State:	// Режим прицеливания
		ResSpeed = MovementSpeedInfo.AimSpeedNormal;
		break;
	case EMovementState::AimWalk_State:	// Режим ходьбы при прицеливании
		ResSpeed = MovementSpeedInfo.AimSpeedWalk;
		break;
	case EMovementState::Walk_State:	// Режим ходьбы
		ResSpeed = MovementSpeedInfo.WalkSpeedNormal;
		break;
	case EMovementState::Run_State:	// Режим бега
		ResSpeed = MovementSpeedInfo.RunSpeedNormal;
		break;

	case EMovementState::SprintRun_State:	// Режим сринта
		if (SprintRunStamina < MovementSpeedInfo.SprintRunSpeedRun)
			ResSpeed = SprintRunStamina;
		else
			ResSpeed = MovementSpeedInfo.SprintRunSpeedRun;
		break;

	default:
		break;
	}

	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

// Получениие состояния жизни
bool AOnePsychoCharacter::GetIsAlive()
{
	return bIsAlive;
}

// Получениие состояния передвижения
EMovementState AOnePsychoCharacter::GetMovementState()
{
	return MovementState;
}

void AOnePsychoCharacter::SetActorRotationByYaw_OnServer_Implementation(float Yaw)
{
	SetActorRotationByYaw_Multicast(Yaw);
}

void AOnePsychoCharacter::SetActorRotationByYaw_Multicast_Implementation(float Yaw)
{
	if (Controller && !Controller->IsLocalPlayerController())
	{
		SetActorRotation(FQuat(FRotator(0.0f, Yaw, 0.0f)));
	}
}

void AOnePsychoCharacter::SetMovementState_OnServer_Implementation(EMovementState NewState)
{
	SetMovementState_Multicast_Implementation(NewState);
}

void AOnePsychoCharacter::SetMovementState_Multicast_Implementation(EMovementState NewState)
{
	MovementState = NewState;
	CharacterUpdate();
}
// ----------------------------------------------------------------------------------------------------



/* ---   Weapon   --- */
// Чтение текущего оружия
AWeaponDefault* AOnePsychoCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}

// Инициализация оружия (На сервере)
void AOnePsychoCharacter::InitWeapon(EWeaponType IdWeapon, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon)
{
	if (CurrentWeapon)
	{
		CurrentWeapon->Destroy();
		CurrentWeapon = nullptr;
	}

	UOnePsychoGameInstance* myGI = Cast<UOnePsychoGameInstance>(GetGameInstance());
	FWeaponInfo myWeaponInfo;
	if (myGI)
	{
		if (myGI->GetWeaponInfoByName(IdWeapon, myWeaponInfo))
		{
			if (myWeaponInfo.WeaponClass)
			{
				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = this;
				SpawnParams.Instigator = GetInstigator();

				AWeaponDefault* myWeapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(myWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myWeapon)
				{
					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					myWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocket_HandR"));
					CurrentWeapon = myWeapon;

					myWeapon->WeaponSetting = myWeaponInfo;
					//myWeapon->AdditionalWeaponInfo.Round = myWeaponInfo.MaxRound;

					myWeapon->ReloadTime = myWeaponInfo.ReloadTime;
					myWeapon->UpdateStateWeapon_OnServer(MovementState);

					myWeapon->AdditionalWeaponInfo = WeaponAdditionalInfo;

					CurrentIndexWeapon = NewCurrentIndexWeapon;//fix

					// Weapon Delegate
					myWeapon->OnWeaponReloadStart.AddDynamic(this, &AOnePsychoCharacter::WeaponReloadStart);
					myWeapon->OnWeaponReloadEnd.AddDynamic(this, &AOnePsychoCharacter::WeaponReloadEnd);
					myWeapon->OnWeaponFireStart.AddDynamic(this, &AOnePsychoCharacter::WeaponFireStart);

					// Удаление неиспользуемых компонентов
					myWeapon->DestroyUnusedComponents();

					if (CurrentWeapon->GetWeaponRound() <= 0 && CurrentWeapon->CheckCanWeaponReload())
						CurrentWeapon->InitReload();

					if (InventoryComponent)
						InventoryComponent->OnWeaponAmmoAvailable.Broadcast(myWeapon->WeaponSetting.WeaponType);
				}
			}
		}
		else
			UE_LOG(LogTemp, Warning, TEXT("AOnePsychoCharacter::InitWeapon - Weapon not found in table -NULL"));
	}
}

// Перезарядка оружия
void AOnePsychoCharacter::TryReloadWeapon()
{
	if (bIsAlive && CurrentWeapon && !CurrentWeapon->bWeaponReloading)
	{
		if (CurrentWeapon->GetWeaponRound() < CurrentWeapon->WeaponSetting.MaxRound && CurrentWeapon->CheckCanWeaponReload())
			CurrentWeapon->InitReload();
	}
}

int32 AOnePsychoCharacter::GetCurrentWeaponIndex()
{
	return CurrentIndexWeapon;
}
// ----------------------------------------------------------------------------------------------------



/* ---   Attack   --- */
// Атака персонажа
void AOnePsychoCharacter::AttackCharEvent(bool bIsFiring)
{
	AWeaponDefault* myWeapon = nullptr;
	myWeapon = GetCurrentWeapon();
	if (myWeapon)
		myWeapon->SetWeaponStateFire_OnServer(bIsFiring);
	else
		UE_LOG(LogTemp, Warning, TEXT("AOnePsychoCharacter::AttackCharEvent - CurrentWeapon -NULL"));
}

// Начало атаки
void AOnePsychoCharacter::InputAttackPressed()
{
	if (bIsAlive)
	{
		AttackCharEvent(true);
	}
}

// Окончание атаки
void AOnePsychoCharacter::InputAttackReleaset()
{
	AttackCharEvent(false);
}

// ----------------------------------------------------------------------------------------------------



/* ---   Aiming   --- */
void AOnePsychoCharacter::InputAimingPressed()
{
	bAimEnabled = true;

	// Обновление статуса прицеливания
	UpdateAimingStatus();
}

void AOnePsychoCharacter::InputAimingReleaset()
{
	bAimEnabled = false;

	// Обновление статуса прицеливания
	UpdateAimingStatus();
}

// Обновление статуса прицеливания
void AOnePsychoCharacter::UpdateAimingStatus()
{
	// Обновление статуса прицеливания для текущего оружия
	CurrentWeapon->SetWeaponStateAimed(bAimEnabled);
	// Обновление анимации на актуальную
	//CurrentWeapon
}
// ----------------------------------------------------------------------------------------------------



/* ---   Health   --- */
float AOnePsychoCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

	if (bIsAlive && !bDebugImmunity)
	{
		CharHealthComponent->ChangeHealthValue(-DamageAmount);
	}

	if (DamageEvent.IsOfType(FRadialDamageEvent::ClassID))
	{
		AProjectileDefault* myProjectile = Cast<AProjectileDefault>(DamageCauser);
		if (myProjectile)
		{
			UTypes::AddEffectBySurfaceType(this, NAME_None, myProjectile->ProjectileSetting.Effect, GetSurfuceType());
		}
	}

	return ActualDamage;
}

void AOnePsychoCharacter::CharDead()
{
	float TimeAnim = 0.0f;
	int32 rnd = FMath::RandHelper(DeadsAnim.Num());

	if (DeadsAnim.IsValidIndex(rnd) && DeadsAnim[rnd] && GetMesh() && GetMesh()->GetAnimInstance())
	{
		TimeAnim = DeadsAnim[rnd]->GetPlayLength();
		GetMesh()->GetAnimInstance()->Montage_Play(DeadsAnim[rnd]);
	}

	bIsAlive = false;

	if (GetController())
	{
		GetController()->UnPossess();
	}

	UnPossessed();

	GetWorldTimerManager().SetTimer(TimerHandle_RagDollTimer, this, &AOnePsychoCharacter::EnableRagdoll, TimeAnim, false);

	GetCursorToWorld()->SetVisibility(false);

	AttackCharEvent(false);

	CharDead_BP();
}

void AOnePsychoCharacter::EnableRagdoll()
{
	if (GetMesh())
	{
		GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		GetMesh()->SetSimulatePhysics(true);
	}
}

/** Реакция на выпадение из мира от UE4 */
/** Reaction to falling out of the world from UE4 */
void AOnePsychoCharacter::FellOutOfWorld(const UDamageType& dmgType)
{
	CharDead();

	Super::FellOutOfWorld(dmgType);
}
// ----------------------------------------------------------------------------------------------------



/* ---   Inventory   --- */
void AOnePsychoCharacter::TrySwicthNextWeapon()
{
	if (CurrentWeapon && !CurrentWeapon->bWeaponReloading && InventoryComponent->WeaponSlots.Num() > 1)
	{
		//We have more then one weapon go switch
		int8 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->AdditionalWeaponInfo;
			if (CurrentWeapon->bWeaponReloading)
				CurrentWeapon->CancelReload();
		}

		if (InventoryComponent)
		{
			if (InventoryComponent->SwitchWeaponToIndex(CurrentIndexWeapon + 1, OldIndex, OldInfo, true))
			{
			}
		}
	}
}

void AOnePsychoCharacter::TrySwitchPreviousWeapon()
{
	if (CurrentWeapon && !CurrentWeapon->bWeaponReloading && InventoryComponent->WeaponSlots.Num() > 1)
	{
		//We have more then one weapon go switch
		int8 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->AdditionalWeaponInfo;
			if (CurrentWeapon->bWeaponReloading)
				CurrentWeapon->CancelReload();
		}

		if (InventoryComponent)
		{
			//InventoryComponent->SetAdditionalInfoWeapon(OldIndex, GetCurrentWeapon()->AdditionalWeaponInfo);
			if (InventoryComponent->SwitchWeaponToIndex(CurrentIndexWeapon - 1, OldIndex, OldInfo, false))
			{
			}
		}
	}
}
// ----------------------------------------------------------------------------------------------------



/* ---   Interface   --- */
void AOnePsychoCharacter::TryAbilityEnabled()
{
	if (AbilityEffect)	// TODO Cool down
	{
		UOP_StateEffect* NewEffect = NewObject<UOP_StateEffect>(this, AbilityEffect);
		if (NewEffect)
		{
			NewEffect->InitObject(this, NAME_None);
		}
	}
}

EPhysicalSurface AOnePsychoCharacter::GetSurfuceType()
{
	EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;
	if (CharHealthComponent)
	{
		if (CharHealthComponent->GetCurrentShield() <= 0)
		{
			if (GetMesh())
			{
				UMaterialInterface* myMaterial = GetMesh()->GetMaterial(0);
				if (myMaterial)
				{
					Result = myMaterial->GetPhysicalMaterial()->SurfaceType;
				}
			}
		}
	}
	return Result;
}

TArray<UOP_StateEffect*> AOnePsychoCharacter::GetAllCurrentEffects()
{
	return Effects;
}

void AOnePsychoCharacter::RemoveEffect(UOP_StateEffect* RemoveEffect)
{
	Effects.Remove(RemoveEffect);
}

void AOnePsychoCharacter::AddEffect(UOP_StateEffect* newEffect)
{
	Effects.Add(newEffect);
}
// ----------------------------------------------------------------------------------------------------


/* ---   NET   --- */
void AOnePsychoCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AOnePsychoCharacter, MovementState);
	DOREPLIFETIME(AOnePsychoCharacter, CurrentWeapon);
}
// ----------------------------------------------------------------------------------------------------