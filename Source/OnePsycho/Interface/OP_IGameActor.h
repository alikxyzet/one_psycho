// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"

#include "OnePsycho/StateEffects/OP_StateEffect.h"

#include "OP_IGameActor.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UOP_IGameActor : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class ONEPSYCHO_API IOP_IGameActor
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	virtual EPhysicalSurface GetSurfuceType();

	virtual TArray<UOP_StateEffect*> GetAllCurrentEffects();
	virtual void RemoveEffect(UOP_StateEffect* RemoveEffect);
	virtual void AddEffect(UOP_StateEffect* newEffect);
};
